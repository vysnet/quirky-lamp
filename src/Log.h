#ifndef LOG_H
#define LOG_H

#include <WString.h>
#include <HardwareSerial.h>

class LogClass {
private:
    constexpr static char INFO[] = "[INFO] ";
    constexpr static char WARN[] = "[WARN] ";
    constexpr static char DEBUG[] = "[DEBUG] ";
    constexpr static char SEPARATOR[] = "====================";

public:
    void infoSeparator();
    template <typename T> void info(T text);
    template <typename T> void infoln(T text);
    void warningSeparator();
    template <typename T> void warning(T text);
    template <typename T> void warningln(T text);
    void debugSeparator();
    template <typename T> void debug(T text);
    template <typename T> void debugln(T text);
};

template <typename T>
void LogClass::info(T text) {
#ifdef LOG_INFO
    Serial.print(String(LogClass::INFO));
    Serial.print(String(text));
#endif
}

template <typename T>
void LogClass::infoln(T text) {
#ifdef LOG_INFO
    Serial.print(String(LogClass::INFO));
    Serial.println(String(text));
#endif
}

template <typename T>
void LogClass::warning(T text) {
#ifdef LOG_WARNING
    Serial.print(String(LogClass::WARN));
    Serial.print(String(text));
#endif
}

template <typename T>
void LogClass::warningln(T text) {
#ifdef LOG_WARNING
    Serial.print(String(LogClass::WARN));
    Serial.println(String(text));
#endif
}

template <typename T>
void LogClass::debug(T text) {
#ifdef LOG_DEBUG
    Serial.print(String(LogClass::DEBUG));
    Serial.print(String(text));
#endif
}

template <typename T>
void LogClass::debugln(T text) {
#ifdef LOG_DEBUG
    Serial.print(String(LogClass::DEBUG));
    Serial.println(String(text));
#endif
}

extern LogClass Log;

#endif //LOG_H
