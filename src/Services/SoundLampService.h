#ifndef __QL_SOUND_LAMP_SERVICE_H__
#define __QL_SOUND_LAMP_SERVICE_H__

#include <memory>
#include <vector>
#include <FastLED.h>

#include "Log.h"
#include "Settings.h"
#include "Services/Service.h"

class SoundLampService : public Service {
    private:
        int switchState;
        int activeMode;
        int baseLevel;
        int samples;
        int maximum;
        int minimum;
        CRGB* leds;
        const std::vector<CRGB> COLOURS{
            CRGB::Black, CRGB::White, CRGB::Yellow,
            CRGB::Pink, CRGB::DarkGreen, CRGB::Purple
        };

        static SoundLampService* singleton;
        SoundLampService();

    protected:
        void run() override final;

    public:
        static SoundLampService* get();
};

#endif
