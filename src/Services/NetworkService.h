#ifndef NETWORK_SERVICE_H 
#define NETWORK_SERVICE_H

#include <map>
#include <WiFi.h>
#include <MQTTClient.h>

#include "Log.h"
#include "Service.h"
#include "Settings.h"

class NetworkService : public Service {
    private:
        std::unique_ptr<WiFiClient> wifi;
        std::unique_ptr<MQTTClient> mqtt;
        std::map <String, String> packets;
        static NetworkService* singleton;

        NetworkService();
        bool connectToNetwork();
        bool connectToBroker();

    protected:
        void run() override final;

    public:
        bool isConnected();
        bool reconnect();
        void sendPacket(const String &topic, const String &payload);
        static NetworkService* get();
};

#endif // NETWORK_SERVICE_H
