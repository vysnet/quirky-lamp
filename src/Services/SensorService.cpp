#include <Arduino.h>
#include "Settings.h"
#include "SensorService.h"

SensorService::SensorService():
    Service(String(F("Sensors")), true) {
    this->network = NetworkService::get();
    this->lcd = std::unique_ptr <LiquidCrystal>(new LiquidCrystal(
                    LCD_RESET, LCD_ENABLE, LCD_D4, LCD_D5, LCD_D6, LCD_D7
                ));
}

const String SensorService::TEMP_TOPIC = String(F("/sens/temp"));
const String SensorService::SOUND_TOPIC = String(F("/sens/sound"));
const String SensorService::LIGHT_TOPIC = String(F("/sens/light"));

SensorService* SensorService::singleton = NULL;
SensorService* SensorService::get() {
    if (SensorService::singleton != NULL) {
        return SensorService::singleton;
    }

    SensorService::singleton = new SensorService();
    return SensorService::singleton;
}

uint32_t SensorService::sample(uint8_t pin) {
    uint32_t sum = 0;
    for (auto i = 0; i < CALIBRATION_SAMPLES; i++) {
        delay(10);
        sum += analogRead(pin);
    }
    return (sum / CALIBRATION_SAMPLES);
}

void SensorService::run() {
    uint32_t temperatureInput = this->sample(TEMP_SENSOR_AO);
    Log.debugln(String(F("Temperature sensor = ")) + String(temperatureInput));
    float temperature = (((temperatureInput / 4095.0)  * 4.0) - 0.42) * 100;
    Log.debugln(String(F("Temperature value = ")) + String(temperature));
    
    uint32_t lightInput = this->sample(LIGHT_SENSOR_AO);
    Log.debugln(String(F("Light sensor = ")) + String(lightInput));
    int light = int(((lightInput / 4095.0) * 100.00));
    Log.debugln(String(F("Light value = ")) + String(light));

    int soundInput = -1, sound = -4095;
    if (digitalRead(SOUND_SENSOR_DO) == HIGH) {
        soundInput = this->sample(SOUND_SENSOR_AO);
        Log.debugln(String(F("Sound sensor = ")) + String(soundInput));
        sound = int(((soundInput / 4095.0) * 100.00));
        Log.debugln(String(F("Sound value = ")) + String(sound));
    }

    // pass the measured sensor data to the network stack
    this->network->sendPacket(SensorService::SOUND_TOPIC, String(sound));
    this->network->sendPacket(SensorService::LIGHT_TOPIC, String(light));
    this->network->sendPacket(SensorService::TEMP_TOPIC, String(temperature));

    this->lcd->begin(16, 2);
    this->lcd->clear();
    this->lcd->setCursor(0, 0);
    String line = String(temperature) + String(" ") 
        + String(sound) + String("% ") + String(light) + String("%");
    this->lcd->print(line.c_str());
    this->lcd->setCursor(0, 1);
    unsigned long s = millis() / 1000;
    String uptime = String("uptime ") + String(s / 3600UL) 
        + String(":") + String((s / 60UL) % 60UL) + String(":") + String(s % 60UL);
    this->lcd->print(uptime);
}
