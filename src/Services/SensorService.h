#ifndef SENSOR_SERVICE_H
#define SENSOR_SERVICE_H

#include <LiquidCrystal.h>

#include "Service.h"
#include "Settings.h"
#include "NetworkService.h"

class SensorService : public Service {
    private:
        static const String TEMP_TOPIC;
        static const String SOUND_TOPIC;
        static const String LIGHT_TOPIC;
        static SensorService* singleton;

        NetworkService* network;
        std::unique_ptr <LiquidCrystal> lcd;

        SensorService();
        uint32_t sample(uint8_t pin);

    protected:
        void run() override final;

    public:
        static SensorService* get();
};

#endif // SENSOR_SERVICE_H
