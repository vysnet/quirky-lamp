#include <Arduino.h>

#include "Log.h"
#include "Settings.h"
#include "Scheduler.h"
#include "Services/SensorService.h"
#include "Services/NetworkService.h"
#include "Services/WatchdogService.h"
#include "Services/SoundLampService.h"

void initHardware() {
    Serial.begin(SERIAL_BAUD);

    Log.infoln(String("Quirky Lamp Booted!"));
    Log.infoSeparator();
    Log.infoln(String("Chip Revision = ") + String(ESP.getChipRevision()));
    Log.infoln(String("Flash Size = ") + String(ESP.getFlashChipSize()) + String("B"));
    Log.infoln(String("Flash Speed = ") + String(ESP.getFlashChipSpeed()) + String("Hz"));
    Log.infoln(String("SDK Version = ") + String(ESP.getSdkVersion()));
    Log.infoln(String("CPU Freq = ") + String(ESP.getCpuFreqMHz()) + String("MHz"));
    Log.infoSeparator();

    // 12 bit wide, 11db high ADC
    analogReadResolution(12);
    analogSetAttenuation(ADC_11db);

    // sensors
    pinMode(LIGHT_SENSOR_AO, INPUT);
    adcAttachPin(LIGHT_SENSOR_AO);
    pinMode(TEMP_SENSOR_AO, INPUT);
    adcAttachPin(TEMP_SENSOR_AO);
    pinMode(SOUND_SENSOR_DO, INPUT);
    pinMode(SOUND_SENSOR_AO, INPUT);
    adcAttachPin(SOUND_SENSOR_AO);

    // reactive hardware
    pinMode(MODE_SWITCH, INPUT);
}

Scheduler* scheduler;

NetworkService* network;
SensorService* sensors;
WatchdogService* watchdog;
SoundLampService* lamp;

void initServices() {
    scheduler = Scheduler::get();

    sensors = SensorService::get();
    network = NetworkService::get();
    watchdog = new WatchdogService();
    lamp = SoundLampService::get(); // disable for networking

    scheduler->addTask((Service*)sensors);
    //scheduler->addTask((Service*)network, 35); // disable for Lamp
    scheduler->addTask((Service*)lamp, REACTIVE_FREQUENCY); // disable for networking
    scheduler->addTask((Service*)sensors, 1 * 60 * 1000);
    scheduler->addTask((Service*)watchdog, 3 * 60 * 1000);
}

void setup() {
    initHardware();
    initServices();
}

void loop() {
    scheduler->run();
}

