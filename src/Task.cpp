#include "Task.h"

Task::Task(Service *service, bool oneShot): 
    Task(service, 0, millis(), true) {}

Task::Task(Service *service, unsigned long period):
    Task(service, period, millis() + period, false) {}

Task::Task(Service *service, unsigned long period, unsigned long deadline):
    Task(service, period, deadline, false) {}

Task::Task(Service *service, unsigned long period,
        unsigned long deadline, bool oneShot) {
    this->service = service;
    this->period = period;
    this->deadline = deadline;
    this->oneShot = oneShot;
}

/**
 * TODO: Possible bug (starvation) when millis() overflows
 * This happens after approx ~50 days runtime
 */
void Task::computeNextDeadline() {
    this->deadline = millis() + this->period;
}

bool Task::operator < (const Task &other) const {
    return this->deadline > other.deadline;
} 

bool Task::operator > (const Task &other) const {
    return this->deadline < other.deadline;
}

bool Task::operator >= (const Task &other) const {
    return this->deadline >= other.deadline;
}

bool Task::operator <= (const Task &other) const {
    return this->deadline <= other.deadline;
}

bool Task::operator == (const Task &other) const {
    return (this->deadline == other.deadline) &&
        (this->oneShot == other.oneShot);
}
