#ifndef SETTINGS_H
#define SETTINGS_H

// Serial monitor baudrate
#ifndef SERIAL_BAUD
    #define SERIAL_BAUD 115200
#endif

// Networking Settings
#ifndef WIFI_SSID
    #error "WiFi SSID missing. Use -DWIFI_SSID=[wifi ssid]"
#endif

#ifndef WIFI_PASSWORD
    #error "WiFi password missing. Use -DWIFI_PASSWORD=[wifi password]"
#endif

#ifndef MQTT_HOST
    #error "MQTT_HOST is not defined. Specify it in the flags.py file."
#endif

#ifndef MQTT_ID
    #define MQTT_ID "quirky-lamp"
#endif

#ifndef MQTT_USER
    #error "MQTT Username missing. Use -DMQTT_USER=[username]"
#endif

#ifndef MQTT_PASSWORD
    #error "MQTT Password missing. Use -DMQTT_PASSWORD=[password]"
#endif

// Sensors collection settings
#ifndef CALIBRATION_SAMPLES
    #define CALIBRATION_SAMPLES 35
#endif

// Reactive lamp service settings
#ifndef REACTIVE_FREQUENCY
    #define REACTIVE_FREQUENCY 3
#endif

#ifndef REACTIVE_LEDS_NUM
    #define REACTIVE_LEDS_NUM 60
#endif

#ifndef REACTIVE_NOISE_THRESHOLD
    #define REACTIVE_NOISE_THRESHOLD 465
#endif

#ifndef REACTIVE_PEAK_THRESHOLD
    #define REACTIVE_PEAK_THRESHOLD 630
#endif

#ifndef REACTIVE_BASELINE_SIZE
    #define REACTIVE_BASELINE_SIZE 10
#endif

#ifndef REACTIVE_BASELINE_HUE
    #define REACTIVE_BASELINE_HUE 130
#endif

// Begin hardware PIN Mappings
#ifndef SOUND_SENSOR_DO
    #define SOUND_SENSOR_DO 34
#endif

#ifndef SOUND_SENSOR_AO
    #define SOUND_SENSOR_AO 35
#endif

#ifndef TEMP_SENSOR_AO
    #define TEMP_SENSOR_AO 32
#endif

#ifndef LIGHT_SENSOR_AO
    #define LIGHT_SENSOR_AO 39
#endif

#ifndef LCD_D7
    #define LCD_D7 33
#endif

#ifndef LCD_D6
    #define LCD_D6 25
#endif

#ifndef LCD_D5
    #define LCD_D5 26
#endif

#ifndef LCD_D4
    #define LCD_D4 27
#endif

#ifndef LCD_ENABLE
    #define LCD_ENABLE 2
#endif

#ifndef LCD_RESET
    #define LCD_RESET 4
#endif

#ifndef LED_STRIP_DIN
    #define LED_STRIP_DIN 16
#endif

#ifndef MODE_SWITCH
    #define MODE_SWITCH 17
#endif
// End hardware PIN Mappings

#endif //SETTINGS_H
