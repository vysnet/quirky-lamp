\documentclass[12pt]{article}

% Packages
\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage{dirtytalk}
\usepackage[square,sort,comma,numbers]{natbib}
\usepackage{url}
\usepackage[table]{xcolor}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{parskip}
\usepackage{fancyhdr}
\usepackage{vmargin}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{pdfpages}
\usepackage{array}
\usepackage{amssymb}
\usepackage{pgfgantt}
\usepackage[official]{eurosym}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage[none]{hyphenat}
\usepackage[ngerman]{datetime}

% Mark the meta information
\title{Quirky Lamp}								
\author{Vasil Sarafov}								


% Set the layout
\makeatletter
\let\thetitle\@title
\let\theauthor\@author
\let\thedate\@date
\makeatother
\graphicspath{{./images}}
\setmarginsrb{3 cm}{2.5 cm}{3 cm}{2.5 cm}{1 cm}{1.5 cm}{1 cm}{1.5 cm}


% Mark the headers and footers
\pagestyle{fancy}
\fancyhf{}
\rhead{\theauthor}
\lhead{\thetitle}
\cfoot{\thepage}

% reformat tables
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}
\newcolumntype{N}{@{}m{0pt}@{}}

% Remove the ugly borders around the hyperlinks
\hypersetup{
    hidelinks=true,
    linkcolor={blue!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

\newenvironment{equationate}{%
 \itemize
 \let\orig@item\item
 \def\item{\orig@item[]\refstepcounter{equation}\def\item{\hfill(\theequation)\orig@item[]\refstepcounter{equation}}}
}{%
 \hfill(\theequation)%
 \enditemize
}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Cover Page %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{titlepage}
	\centering
    \includegraphics[scale = 0.35]{images/tum-logo.png}\\[1.0 cm]
    \textsc{\LARGE Technische Universität München}\\[0.25 cm]
    \textsc{Department of Informatics}\\[1.0 cm]
	\textsc{\Large Real-Time Systems (IN2060)}\\[0.25 cm]
	\textsc{Project Report}\\[0.5 cm]
	
	\rule{\linewidth}{0.2 mm} \\[0.4 cm]
	{ \huge \bfseries \thetitle}\\
	\rule{\linewidth}{0.2 mm} \\[1.5 cm]
	
	\textsc{Author}\\[0.10 cm]
	\textsl{Vasil Sarafov (sarafov@cs.tum.edu)}\\[0.6 cm]
	
    \textsc{Lecturer}\\[0.10 cm]
	\textsl{Dr. Alexander Lenz (alex.lenz@tum.de)}\\[1 cm]	
	
	\today\\[0.10 cm]
	\textsc{Winter Term 17/18}
\end{titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Table of contents %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\tableofcontents
\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{sec:introduction}
The following document provides a not so formal description
of a simple IoT powered lamp that was
created as part of a student project for the Real-Time Systems lecture,
given by Dr.Alexander Lenz at the Technical University of Munich during
the 2017/18 Winter Term.

The described system is called ``Quirky Lamp'' and 
utilizes the \textbf{Wi-Fi} and Bluetooth capable
\textbf{ESP32} system on chip (SoC).
It collects data from a photo resistor, sound and temperature \textbf{sensors}
and distributes it via \textbf{MQTT}. A multicoloured lightning
from an \textbf{LED strip} provides the basic lamp functionality. In addition to that,
the lamp is capable of dynamically adjusting its lightning based on sound noises
(e.g. reacting in real time on music).

The reason for creating this project is purely educational. I wanted
to evaluate the ESP32 chip as I have experience with its predecessor - the ESP8266.
The project does not solve any significant (or real) problem but rather
aspires to be an engineering product that brings enjoyment to its creator.

The document is structured as follows. Sections \ref{sec:hardware}
and \ref{sec:software} provide an overview of
the hardware configuration
and the software stack respectively.
Chapter \ref{sec:further-work} describes known issues with the current
state of the system and suggests improvements.
Section \ref{sec:additiona-materials} outlines additional materials such as
references to the actual code base, pictures and demonstration videos
of the system.

\section{Hardware}
\label{sec:hardware}
In this Chapter we provide a more detailed description of the hardware components
that were utilized in the project. Section \ref{sec:hardware-components} lists
every single one of them and Section \ref{sec:electronics-schematics} depicts
the corresponding electronics schematics.

\subsection{Components}
\label{sec:hardware-components}

At the core of the system is the \textbf{ESP32} SoC.
It is an upgraded successor of the very popular ESP8266 chip.
ESP32 is created by the Chinese manufacturer Espressif Systems and is a low-power, low-cost 
Wi-Fi and dual-mode Bluetooth capable SoC. At its heart are a dual-core
Tensilica Xtensa LX6 processor with a clock rate of up to 240 MHz, 448 KiB of ROM,
520 KiB of SRAM, up to 4 MiB embedded flash memory, Wi-Fi, Bluetooth modules
and cryptographic hardware acceleration.
Additional information with references to the original
manufacturing datasheet can be found in \cite{esp32-net}. In our project
we utilized the official ESP32 Core development module which provides
an easy USB-to-Serial programming interface.

A simple photo resistor is used for measuring the \emph{surrounding} lightning level
(the measured value does not affect the lamp functionality).
The temperature is measured by a TMP36 sensor and the sound level is captured
by a KY-037 sensor.

The lightning functionality of the lamp is achieved via a WS2812B-based strip
with 60 individually addressable LEDs.
With a push button the user is able to iterate through
the different working modes of the lamp. Currently there are 7:
\begin{itemize}
    \item Disabled - the lamp is not active, i.e. the LED strip is turned off
    \item White lightning - all 60 LEDs glow in white colour
    \item Yellow lightning - all 60 LEDs glow in yellow colour
    \item Pink lightning - all 60 LEDs glow in pink colour
    \item Green lightning - all 60 LEDs glow in green colour
    \item Purple lightning - all 60 LEDs glow in purple colour
    \item Reactive lightning - the colour and number of active LEDs is determined by the measured sound level
\end{itemize}

To provide a better user experience,
an ADM1602K 16x2 LCD monitor is used to display the collected
sensor data and the time elapsed since the system was booted.
Data from the ESP32 is transferred to the LCD's microcontroller using 
a 4-bit data bus.

% http://ctms.engin.umich.edu/CTMS/Content/Activities/TMP35_36_37.pdf
% https://www.sparkfun.com/datasheets/LCD/ADM1602K-NSW-FBS-3.3v.pdf

\subsection{Electronics Schematics}
\label{sec:electronics-schematics}
The electronics schematics were created using the open source
KiCad Software \cite{kicad}.

\pagebreak

\vspace*{-2.5cm}
\hspace*{-1.5cm}
\includegraphics[scale=0.85,angle=90,origin=c]
{../hardware-schematics/quirky-lamp.pdf}

\section{Software}
\label{sec:software}

\subsection{Baseline}
\label{sec:software-baseline}
The baseline of the software stack, which the project's firmware is built on top of,
is the official ESP32 IoT Development Framework Software Development Kit (SDK)
provided by the manufacturer.
It is a C code base, large part of which is closed source, that provides the core
software components needed for interacting with the chip. This includes the
hardware abstraction layer (HAL), a bootloader, a TCP/IP stack based on lwIP,
core C and cryptographic libraries, a memory allocator,
a file system implementation and many more.
For more information about the SDK, please refer
to its repository \cite{esp32-idf-sdk}.

In addition to that, Espressif Systems provides a C++ interface to its SDK
which implements the Wiring framework \cite{wiring-framework, esp32-arduino}.
This allows ESP32 projects to benefit from the large variety of arduino libraries
and is exactly what our system does. 

On top of the C++ Wiring interface sits our firmware. In the next section we will
take a closer look of its architecture.

% http://wiring.org.co/reference/
% http://savannah.nongnu.org/projects/lwip/
% https://github.com/espressif/esp-idf
% https://github.com/espressif/arduino-esp32

\subsection{Architecture}

\label{sec:software-architecture}
The architecture of the firmware is rather simple. It consists
of \emph{services} (Section \ref{sec:software-services})
which are the basic execution units, i.e. they are
what processes are on a typical operating system without the isolation
and protection mechanisms between them. Our services provide a common
execution interface and are used to separate and easily reuse
different business logic across the project.

A scheduler (Section \ref{sec:software-scheduler}) is utilized to organize
the execution order of the services and meet their timing requirements.

A serial interface logger (Section \ref{sec:software-logger}) allows
the developer to keep track of the firmware's action in real time.

\subsubsection{Services}
\label{sec:software-services}
Currently there are 4 active services which all derive from the base service class
to provide a common API.

When started, the \textbf{network service} is responsible
for establishing and maintaining a WiFi connection to a
network whose credentials are specified at compile time. When executed,
the service sends all queued data packets to an MQTT message broker.
Message Queuing Telemetry Transport (MQTT) is a standardised and 
very lightweight data networking protocol which is based on the
publish-subscribe communication pattern. It sits on top of TCP and and provides
3 different quality of service (QoS) levels. Because of its small overhead, easy client
implementation and
ability to provide asynchronous communication between peers in a distributed network,
MQTT is very suitable for embedded devices.
For more information about MQTT refer to the official specification \cite{mqtt-specs}.
A deeper analysis of MQTT's overhead and comparison with other IoT communication
protocols can be found in \cite{iot-protocol-overhead}.

In our case a self hosted MQTT message broker was used which is available
at \textit{mqtt.sarafov.net}. It takes advantage of the Mosquitto project
\cite{mosquitto}.
The captured data can be then viewed from any MQTT client which can connect
to the broker, provided that it has the needed authentication credentials.
We used a simple MQTT client for android to remotely inspect the data.

The \textbf{sensor service} collects data from the temperature and sound sensors and
reads the level of the photoresistor. The measured values are then displayed on the LCD
monitor and are passed to the network service in order to be dispatched
to the MQTT message broker.

The \textbf{sound lamp service} controls the LED lamp.
When executed it polls the status of the push button data pin to register whether
the user has changed the lamp's active mode (described in Section \ref{sec:hardware-components}). If the reactive lightning mode is activated, the service
measures the sound level and turns on the appropriate amount of LEDs.

The \textbf{watchdog service} periodically monitors
the amount free heap memory space. If it is critically low, it sends
a notification to the MQTT message broker via the networking service.
It is helpful for detecting unwanted memory leaks.

\subsubsection{Scheduler}
\label{sec:software-scheduler}
The scheduler creates \emph{tasks} from the services that need to run.
A \emph{task} has a starting time and can be either one-shot, meaning it must be
executed only once, or periodic, meaning that it needs to be executed periodically.
The implemented scheduler is non-preemptive and works on the ``earliest starting time 
first'' strategy.

\subsubsection{Serial Logger}
\label{sec:software-logger}
For developing purposes a serial logger was implemented. It is used
to display useful information to the developer when the system
is attached to a serial monitor. The developer can then keep better
track of what is currently happening on the microcontroller.

At the moment the logger has three different levels of verbosity which
can be activated/deactivated separately at compile time:
\begin{enumerate}
    \item Information - for general clear text messages
    \item Warning - to signal events that require the developer's attention and shouldn't normally occur
    \item Debug - to inspect the content of internal variables/data structures
\end{enumerate}


\subsection{Toolchain}
\label{sec:software-toolchain}
The Quirky Lamp project takes advantage of the PlatformIO \textbf{ecosystem} \cite{platformio}.
PlatformIO is an open source product which provides a unified,
highly extensible and fully automatized development environment for programming 
different microcontrollers. It abstracts the typical and very often
cumbersome toolchain setup process
that a developer must execute to start programming on a specific microcontroller.
It solves this problem by providing ready configurations for popular chips and
development boards. In addition to that, a library manager, cross-platform IDE,
unit testing framework and unified debugger are available as separate components. 

In our case, we use PlatformIO Core to install, configure and easily utilize
the xtensa-gcc toolchain, which is the closed source ESP32 toolchain
provided by the chip manufacturer. Furthermore the library manager is used to 
automatically install some of the project dependencies. For more information please
refer to Section \ref{sec:additiona-materials}.

\section{Further Work}
\label{sec:further-work}
The project has successfully fulfilled its initial requirements. However,
there are some known issues and a lot of potential for further improvements.

The ESP32 analog-to-digital-converter (ADC) accuracy is not high and
surprisingly varies on each device. The
conversion function is neither linear nor
monotonically increasing as it should be \cite{esp32-adc-accuracy}.
This implies a good correction algorithm for 
mapping the sensor ADC data to real values. The current implementation presents a
rather bad one (the mapping function was experimentally determined and is valid
only for certain value ranges).

As mentioned in Chapter \ref{sec:software-baseline} the MQTT connection
to the broker is not encrypted. This can be further improved by adding/enabling
TLS both device and server sides.

The collected lightning data is not very accurate since a photoresistor is used.
This can be enhanced by utilizing a decent light sensor.

The Wiring Framework interface, which is used by this project, takes advantage
only of a single CPU core of the ESP32 chip. The used LED strip drivers utilize
of busy loops to synchronize the main controller with the strip's microcontroller.
The busy waiting causes the SDK's watchdog to reboot the system when a hard WiFi-Stack
deadline is missed (happens nondeterministically). Because of this the networking
and sound lamp services  (Section \ref{sec:software-services})
conflict with one another. A temporarily implemented workaround is to disable the MQTT 
message publications when the lamp is active and disable the sound lamp service when 
MQTT messages must be distributed. A permanent solution to the problem
is to swap out the used LED strip driver with one which synchronizes the communication
asynchronously via interrupts. Taking advantage of the second CPU core is not possible
when the Wiring Framework interface is used.

Instead of displaying the elapsed time since the system was booted on the LCD monitor,
the correct time of day can be shown. This can be achieved by utilizing the 
Simple Network Time Protocol (SNTP).
 
\section{Additional Materials}
\label{sec:additiona-materials}
The additionally provided materials are:
\begin{itemize}
    \item the firmware codebase (with references to the used third-party libraries)
    \item the electronics schematics design files
    \item the \LaTeX code for this report
\end{itemize}
and can be found on the project's git repository: \href{https://gitlab.com/v45k0/quirky-lamp}{https://gitlab.com/v45k0/quirky-lamp}.

Pictures of the final system and demonstration videos are available on\\
\href{https://cloud.sarafov.net/s/pQDZIBtIGIHIl7m}{https://cloud.sarafov.net/s/pQDZIBtIGIHIl7m}

\begin{thebibliography}{99}

\bibitem{iot-protocol-overhead}
    \href{https://gitlab.com/v45k0/iot-data-protocols/raw/master/paper/seminar.pdf}{Comparison of IoT Data Protocol Overhead} \\
Vasil Sarafov, Jan Seeger - Seminar Future Internet SS17, Technical University of Munich:
http://bit.ly/2E8Mtjr


\bibitem{mqtt-specs}
    \href{http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html}{MQTT Version 3.1.1, OASIS Standard, 29 October 2014} \\
MQTT is a Client Server publish/subscribe messaging transport protocol. It is light weight, open, simple, and designed so as to be easy to implement

\bibitem{mosquitto}
    \href{https://mosquitto.org/}{Mosquitto} \\
Eclipse Mosquitto™ is an open source (EPL/EDL licensed) message broker that implements the MQTT protocol versions 3.1 and 3.1.1. 

\bibitem{platformio}
    \href{http://platformio.org/}{http://platformio.org/} \\
An open source ecosystem for IoT development. Cross-platform IDE and unified debugger. Remote unit testing and firmware updates

\bibitem{esp32-net}
    \href{http://esp32.net/}{http://esp32.net/} \\
A community driven collection of resources for the ESP32 chip

\bibitem{kicad}
    \href{http://kicad-pcb.org/}{http://kicad-pcb.org/} \\
A Cross Platform and Open Source Electronics Design Automation Suite

\bibitem{esp32-adc-accuracy}
    \href{https://github.com/espressif/esp-idf/issues/164}{https://github.com/espressif/esp-idf/issues/164} \\
ESP32 ADC accuracy issue

\bibitem{esp32-idf-sdk}
    \href{https://github.com/espressif/esp-idf}{https://github.com/espressif/esp-idf} \\
Espressif IoT Development Framework. Official development framework for ESP32.


\bibitem{esp32-arduino}
    \href{http://wiring.org.co/reference/}{http://wiring.org.co/reference/} \\
Arduino core for the ESP32

\bibitem{wiring-framework}
    \href{https://github.com/espressif/arduino-esp32}{https://github.com/espressif/arduino-esp32} \\
Wiring is an open-source programming framework for microcontrollers.

\bibitem{esp32-adc-accuracy}
    \href{https://github.com/espressif/esp-idf/issues/164}{https://github.com/espressif/esp-idf/issues/164} \\
ESP32 analog-to-digital-converter accuracy issue
	
\end{thebibliography}
\end{document}
